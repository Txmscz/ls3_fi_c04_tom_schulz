import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class Form_aendern extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JLabel lblTestText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form_aendern frame = new Form_aendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Form_aendern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 565);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblTestText = new JLabel("Dieser Text soll ver\u00E4ndert werden");
		lblTestText.setBounds(103, 24, 234, 20);
		contentPane.add(lblTestText);
		
		JLabel lblNewLabel = new JLabel("Aufgabe 1: Hintergrundfarge \u00E4ndern");
		lblNewLabel.setBounds(10, 55, 258, 14);
		contentPane.add(lblNewLabel);
		
		JLabel label = new JLabel("Aufgabe 2: Text formatieren ");
		label.setBounds(10, 148, 188, 14);
		contentPane.add(label);
		
		JButton button_5 = new JButton("Arial");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTestText.setFont(new Font("Arial", Font.PLAIN, 12));
			}
		});
		button_5.setBounds(10, 173, 119, 23);
		contentPane.add(button_5);
		
		JButton button_6 = new JButton("Comic Sans MS");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblTestText.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		button_6.setBounds(133, 173, 119, 23);
		contentPane.add(button_6);
		
		JButton button_7 = new JButton("Courier New");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTestText.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		button_7.setBounds(255, 173, 119, 23);
		contentPane.add(button_7);
		
		textField = new JTextField();
		textField.setBounds(10, 207, 364, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		
		JButton button_8 = new JButton("Ins Label schreiben ");
		button_8.setBounds(10, 238, 180, 23);
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTestText.setText(textField.getText());
			}
		});
		contentPane.add(button_8);
		
		JButton button_9 = new JButton("Text im Label l\u00F6schen");
		button_9.setBounds(194, 238, 180, 23);
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTestText.setText("");
			}
		});
		contentPane.add(button_9);
		
		JLabel label_1 = new JLabel("Aufgabe 3: Schriftfrabe \u00E4ndern");
		label_1.setBounds(10, 272, 188, 14);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		label_2.setBounds(10, 331, 188, 14);
		contentPane.add(label_2);
		
		JButton button_13 = new JButton("+");
		button_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTestText.setFont(new Font(lblTestText.getFont().getFontName(), Font.PLAIN, lblTestText.getFont().getSize()+1));

			}
		});
		button_13.setBounds(10, 356, 180, 23);
		contentPane.add(button_13);
		
		JButton button_14 = new JButton("-");
		button_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTestText.setFont(new Font(lblTestText.getFont().getFontName(), Font.PLAIN, lblTestText.getFont().getSize()-1));
			}
		});
		button_14.setBounds(194, 356, 180, 23);
		contentPane.add(button_14);
		
		JLabel label_3 = new JLabel("Aufgabe 5: Textausrichten");
		label_3.setBounds(10, 390, 188, 14);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("Aufgabe 6: Programm beenden ");
		label_4.setBounds(10, 449, 188, 14);
		contentPane.add(label_4);
		
		JButton button_18 = new JButton("Exit");
		button_18.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		button_18.setBounds(10, 474, 364, 41);
		contentPane.add(button_18);
		
		JButton button_2 = new JButton("Gelb");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.YELLOW);
			}
		});
		button_2.setBounds(10, 114, 119, 23);
		contentPane.add(button_2);
		
		JButton button_3 = new JButton("Standardfarbe");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(new Color(0xEEEEEE));
			}
		});
		button_3.setBounds(133, 114, 119, 23);
		contentPane.add(button_3);
		
		JButton button_4 = new JButton("Farbe w\u00E4hlen");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button_4.setBounds(255, 114, 119, 23);
		contentPane.add(button_4);
		
		JButton button = new JButton("Rot");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.RED);
			}
		});
		button.setBounds(10, 80, 119, 23);
		contentPane.add(button);
		
		JButton button_1 = new JButton("Gr\u00FCn");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(Color.GREEN);
			}
		});
		button_1.setBounds(133, 80, 119, 23);
		contentPane.add(button_1);
		
		JButton button_19 = new JButton("Blau");
		button_19.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.BLUE);
			}
		});
		button_19.setBounds(255, 80, 119, 23);
		contentPane.add(button_19);
		
		JButton button_10 = new JButton("Rot");
		button_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTestText.setForeground(Color.RED);
			}
		});
		button_10.setBounds(10, 297, 119, 23);
		contentPane.add(button_10);
		
		JButton button_11 = new JButton("Blau");
		button_11.setBounds(133, 297, 119, 23);
		button_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTestText.setForeground(Color.BLUE);
			}
		});
		contentPane.add(button_11);
		
		JButton button_12 = new JButton("Schwarz");
		button_12.setBounds(255, 297, 119, 23);
		button_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTestText.setForeground(Color.BLACK);
			}
		});
		contentPane.add(button_12);
		
		JButton button_15 = new JButton("linksb\u00FCndig");
		button_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTestText.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		button_15.setBounds(10, 415, 119, 23);
		contentPane.add(button_15);
		
		JButton button_16 = new JButton("zentriert");
		button_16.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTestText.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		button_16.setBounds(133, 415, 119, 23);
		contentPane.add(button_16);
		
		JButton button_17 = new JButton("rechtsb\u00FCndig");
		button_17.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTestText.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		button_17.setBounds(255, 415, 119, 23);
		contentPane.add(button_17);
	}
}
