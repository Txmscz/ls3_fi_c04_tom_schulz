package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.User;
import model.persistance.IUserPersistance;
import model.persistanceDB.AccessDB;

/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {
	private AlienDefenceController alienDefenceController;
//	private AccessDB dbAccess;

	private IUserPersistance userPersistance;
	
	public UserController(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
//		this.dbAccess = dbAccess;
	}
	
	public void createUser(User user) {
		
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username) {
		 User currentUser = this.userPersistance.readUser(username);
		 return currentUser;
	}
	
	public void changeUser(User user) {
		
	}
	
	public void deleteUser(User user) {
		
	}
	
	public boolean checkPassword(String username, String passwort) {
		return false;
	}
}
